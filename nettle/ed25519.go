package nettle

/*
#cgo LDFLAGS: -lhogweed -lnettle -lgmp
#include <nettle/eddsa.h>
void
ed25519_sha512_public_key (uint8_t *pub, const uint8_t *priv);

void
ed25519_sha512_sign (const uint8_t *pub,
		     const uint8_t *priv,
		     size_t length, const uint8_t *msg,
		     uint8_t *signature);

int
ed25519_sha512_verify (const uint8_t *pub,
		       size_t length, const uint8_t *msg,
		       const uint8_t *signature);
*/
import "C"

import "unsafe"

type Signer struct {
	priv [32]byte
	pub  [32]byte
}

func NewSigner(priv [32]byte) *Signer {
	var pub [32]byte
	C.ed25519_sha512_public_key((*C.uint8_t)(unsafe.Pointer(&pub[0])),
		(*C.uint8_t)(unsafe.Pointer(&priv[0])))
	return &Signer{priv: priv, pub: pub}
}

func (s *Signer) Sign(msg []byte) (signature [64]byte) {
	size := len(msg)
	if len(msg) == 0 {
		msg = make([]byte, 1)
	}
	var buffer [64]byte
	C.ed25519_sha512_sign((*C.uint8_t)(unsafe.Pointer(&s.pub[0])),
		(*C.uint8_t)(unsafe.Pointer(&s.priv[0])),
		C.size_t(size), (*C.uint8_t)(unsafe.Pointer(&msg[0])),
		(*C.uint8_t)(unsafe.Pointer(&buffer[0])))
	copy(signature[:], buffer[:])
	return
}

func (s *Signer) Public() [32]byte {
	return s.pub
}

func Verify(pub [32]byte, msg []byte, signature [64]byte) bool {
	size := len(msg)
	if len(msg) == 0 {
		msg = make([]byte, 1)
	}
	return C.int(1) ==
		C.ed25519_sha512_verify((*C.uint8_t)(unsafe.Pointer(&pub[0])),
			C.size_t(size), (*C.uint8_t)(unsafe.Pointer(&msg[0])),
			(*C.uint8_t)(unsafe.Pointer(&signature[0])))
}
