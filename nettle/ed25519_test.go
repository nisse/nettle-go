package nettle

import (
	"bytes"
	"crypto"
	libEd25519 "crypto/ed25519"
	"encoding/hex"
	"fmt"
	"testing"
)

func fromHex(s string) []byte {
	b, err := hex.DecodeString(s)
	if err != nil {
		panic(fmt.Sprintf("%v", err))
	}
	return b
}

func fromHex32(s string) (p [32]byte) {
	b := fromHex(s)
	if len(b) != 32 {
		panic("bad length")
	}
	copy(p[:], b)
	return
}
func fromHex64(s string) (p [64]byte) {
	b := fromHex(s)
	if len(b) != 64 {
		panic("bad length")
	}
	copy(p[:], b)
	return
}

func TestEd25519(t *testing.T) {
	test_one := func(privHex, pubHex, msgHex, signatureHex string) {
		priv := fromHex32(privHex)
		refPub := fromHex32(pubHex)
		msg := fromHex(msgHex)
		refSignature := fromHex64(signatureHex)
		signer := NewSigner(priv)

		if pub := signer.Public(); !bytes.Equal(pub[:], refPub[:]) {
			t.Errorf("Inconsistent public key.\n  exp: %x\n  got:%x\n",
				refPub, pub)
		}

		if signature := signer.Sign(msg); !bytes.Equal(signature[:], refSignature[:]) {
			t.Errorf("Sign failed.\n  private: %x\n  msg: %x\n  exp: %x\n  got:%x\n",
				priv, msg, refSignature, signature)
		}
		if !Verify(refPub, msg, refSignature) {
			t.Errorf("Verify failed.\n  pub: %x\n  msg: %x\n  sig: %x", refPub, msg, refSignature)
		}
		var clobberedMsg []byte
		if len(msg) > 0 {
			clobberedMsg = msg[:len(msg)-1]
		} else {
			clobberedMsg = []byte("x")
		}
		if Verify(refPub, clobberedMsg, refSignature) {
			t.Errorf("Verify succeeded on clobbered message.\n  pub: %x\n  msg: %x\n  sig: %x",
				refPub, clobberedMsg, refSignature)
		}
	}
	test_one("9d61b19deffd5a60ba844af492ec2cc44449c5697b326919703bac031cae7f60",
		"d75a980182b10ab7d54bfed3c964073a0ee172f3daa62325af021a68f707511a",
		"",
		"e5564300c360ac729086e2cc806e828a84877f1eb8e5d974d873e065224901555fb8821590a33bacc61e39701cf9b46bd25bf5f0595bbe24655141438e7a100b")
	test_one("4ccd089b28ff96da9db6c346ec114e0f5b8a319f35aba624da8cf6ed4fb8a6fb",
		"3d4017c3e843895a92b70aa74d1b7ebc9c982ccf2ec4968cc0cd55f12af4660c",
		"72",
		"92a009a9f0d4cab8720e820b5f642540a2b27b5416503f8fb3762223ebdb69da085ac1e43e15996e458f3613d0f11d8c387b2eaeb4302aeeb00d291612bb0c00")
	test_one("c5aa8df43f9f837bedb7442f31dcb7b166d38535076f094b85ce3a2e0b4458f7",
		"fc51cd8e6218a1a38da47ed00230f0580816ed13ba3303ac5deb911548908025",
		"af82",
		"6291d657deec24024827e69c3abe01a30ce548a284743a445e3680d7db5ac3ac18ff9b538d16f290ae67f760984dc6594a7c15e9716ed28dc027beceea1ec40a")
	test_one("0d4a05b07352a5436e180356da0ae6efa0345ff7fb1572575772e8005ed978e9",
		"e61a185bcef2613a6c7cb79763ce945d3b245d76114dd440bcf5f2dc1aa57057",
		"cbc77b",
		"d9868d52c2bebce5f3fa5a79891970f309cb6591e3e1702a70276fa97c24b3a8e58606c38c9758529da50ee31b8219cba45271c689afa60b0ea26c99db19b00c")
}

func BenchmarkEd25519Sign(b *testing.B) {
	var key [32]byte
	signer := NewSigner(key)

	for i := 0; i < b.N; i++ {
		signature := signer.Sign(key[:])
		_ = signature
	}
}

func BenchmarkEd25519Verify(b *testing.B) {
	pub := fromHex32("e61a185bcef2613a6c7cb79763ce945d3b245d76114dd440bcf5f2dc1aa57057")
	msg := fromHex("cbc77b")
	signature := fromHex64("d9868d52c2bebce5f3fa5a79891970f309cb6591e3e1702a70276fa97c24b3a8e58606c38c9758529da50ee31b8219cba45271c689afa60b0ea26c99db19b00c")
	for i := 0; i < b.N; i++ {
		valid := Verify(pub, msg, signature)
		if !valid {
			panic("invalid signature")
		}
	}
}

func BenchmarkLibEd25519Sign(b *testing.B) {
	var key [32]byte
	signer := libEd25519.NewKeyFromSeed(key[:])

	for i := 0; i < b.N; i++ {
		signature, err := signer.Sign(nil, key[:], crypto.Hash(0))
		_ = signature
		if err != nil {
			panic("sign failed")
		}
	}
}

func BenchmarkLibEd25519Verify(b *testing.B) {
	pub := fromHex32("e61a185bcef2613a6c7cb79763ce945d3b245d76114dd440bcf5f2dc1aa57057")
	msg := fromHex("cbc77b")
	signature := fromHex64("d9868d52c2bebce5f3fa5a79891970f309cb6591e3e1702a70276fa97c24b3a8e58606c38c9758529da50ee31b8219cba45271c689afa60b0ea26c99db19b00c")
	for i := 0; i < b.N; i++ {
		valid := libEd25519.Verify(pub[:], msg, signature[:])
		if !valid {
			panic("invalid signature")
		}
	}
}
