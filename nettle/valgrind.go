package nettle

/*
#include <valgrind/memcheck.h>
#include <valgrind/valgrind.h>
#include <stdint.h>

void
mark_bytes_undefined (size_t size, const uint8_t *p)
{
  VALGRIND_MAKE_MEM_UNDEFINED(p, size);
}
void
mark_bytes_defined (size_t size, const uint8_t *p)
{
  VALGRIND_MAKE_MEM_DEFINED(p, size);
}
*/
import "C"

import "unsafe"

func MarkUndefined(data []byte) {
	C.mark_bytes_undefined(C.size_t(len(data)), (*C.uint8_t)(unsafe.Pointer(&data[0])))
}

func MarkDefined(data []byte) {
	C.mark_bytes_defined(C.size_t(len(data)), (*C.uint8_t)(unsafe.Pointer(&data[0])))
}
