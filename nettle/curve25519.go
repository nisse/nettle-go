package nettle

/*
#cgo LDFLAGS: -lhogweed -lnettle -lgmp
#include <nettle/curve25519.h>
void
curve25519_mul_g (uint8_t *q, const uint8_t *n);

void
curve25519_mul (uint8_t *q, const uint8_t *n, const uint8_t *p);
*/
import "C"

import "unsafe"

func Curve25519Mul(n [32]byte, p [32]byte) [32]byte {
	var q [32]byte
	C.curve25519_mul((*C.uint8_t)(unsafe.Pointer(&q[0])),
		(*C.uint8_t)(unsafe.Pointer(&n[0])),
		(*C.uint8_t)(unsafe.Pointer(&p[0])))
	return q
}

func Curve25519MulG(n [32]byte) [32]byte {
	var q [32]byte
	C.curve25519_mul_g((*C.uint8_t)(unsafe.Pointer(&q[0])),
		(*C.uint8_t)(unsafe.Pointer(&n[0])))
	return q
}
