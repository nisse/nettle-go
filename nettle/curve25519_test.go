package nettle

import (
	"bytes"
	"testing"
)

func TestCurve25519MulG(t *testing.T) {
	for _, item := range [][][32]byte{
		// Input-output pairs from RFC 7748 DH test vectors.
		{
			fromHex32("77076d0a7318a57d3c16c17251b26645df4c2f87ebc0992ab177fba51db92c2a"),
			fromHex32("8520f0098930a754748b7ddcb43ef75a0dbf3a0d26381af4eba4a98eaa9b4e6a"),
		},
		{
			fromHex32("5dab087e624a8a4b79e17f8b83800ee66f3bb1292618b6fd1c2f8b27ff88e0eb"),
			fromHex32("de9edb7d7b7dc1b4d35b61c2ece435373f8343c85b78674dadfc7e146f882b4f")},
	} {
		q := Curve25519MulG(item[0])
		if !bytes.Equal(q[:], item[1][:]) {
			t.Errorf("Curve25519MulG failed:\n  n: %x\n  P: %x (bad)\nexp:  %x\n",
				item[0], q, item[1])
		}
	}
}

func TestCurve25519Mul(t *testing.T) {
	for _, item := range [][][32]byte{
		// n, P, Q, n P = Q values from RFC 7748 DH test vectors.
		{
			fromHex32("77076d0a7318a57d3c16c17251b26645df4c2f87ebc0992ab177fba51db92c2a"),
			fromHex32("de9edb7d7b7dc1b4d35b61c2ece435373f8343c85b78674dadfc7e146f882b4f"),
			fromHex32("4a5d9d5ba4ce2de1728e3bf480350f25e07e21c947d19e3376f09b3c1e161742"),
		},
		{
			fromHex32("5dab087e624a8a4b79e17f8b83800ee66f3bb1292618b6fd1c2f8b27ff88e0eb"),
			fromHex32("8520f0098930a754748b7ddcb43ef75a0dbf3a0d26381af4eba4a98eaa9b4e6a"),
			fromHex32("4a5d9d5ba4ce2de1728e3bf480350f25e07e21c947d19e3376f09b3c1e161742"),
		},
	} {
		q := Curve25519Mul(item[0], item[1])
		if !bytes.Equal(q[:], item[2][:]) {
			t.Errorf("Curve25519MulG failed:\n  n: %x\n  P: %x\n  Q: %x (bad)\nexp:  %x\n",
				item[0], item[1], q, item[2])
		}
	}
}

func BenchmarkCurve25519Mul(b *testing.B) {
	var n [32]byte
	var p [32]byte
	for i := 0; i < b.N; i++ {
		q := Curve25519Mul(n, p)
		_ = q
	}
}

func BenchmarkCurve25519MulG(b *testing.B) {
	var n [32]byte
	for i := 0; i < b.N; i++ {
		q := Curve25519MulG(n)
		_ = q
	}
}
